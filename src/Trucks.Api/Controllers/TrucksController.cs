﻿using Microsoft.AspNetCore.Mvc;

namespace Trucks.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TrucksController : ControllerBase
    {
        public TrucksController()
        {
        }

        [HttpGet]
        public object Get()
        {
            return new { Message = "project structure" };
        }
    }
}
